# import yaml
#-*- coding: utf-8 -*-
# # with open('XXX.yaml') as f:
#         print yaml.safe_load(f)
# with open("XXX.yaml") as f:
#     profile = yaml.safe_load(f)
    # print profile["bob"]
    # print profile['bob']['age']
    # print profile['alice']
    # print profile['alice']['color']
# for name in profile.keys():
# 	if name == 'kik':
# 		print profile[name]
# import time
# import threading
# def cal_max(numbers, delay):
#         time.sleep(delay)
#         print 'Max:', max(numbers)
#
# def cal_min(numbers, delay):
#         time.sleep(delay)
#         print 'Min:', min(numbers)
# start_time = time.time()
# arr = [10, 7, 29, 62, 19]
# delay1 = 3
# delay2 = 5
# cal_max(arr, delay1)
# cal_min(arr, delay2)
# print 'Elapsed time:', time.time() - start_time
# thread1 = threading.Thread(target=cal_max, args=(arr, delay1,))
# thread2 = threading.Thread(target=cal_min, args=(arr, delay2,))
# thread1.start()
# thread2.start()
# thread1.join()
# thread2.join()
# print 'Elapsed time:', time.time() - start_time
# -----------------------------Exercise-------------------------------------
import time
import threading

def cal_max(numbers, delay):
        time.sleep(delay)
        print 'Square:', (numbers)

def cal_min(numbers, delay):
        time.sleep(delay)
        print 'Cube :', (numbers)
delay1 = 2
delay2 = 3
num_1 = input("ด้าน")
start_time = time.time()
num = num_1*num_1
print ("Square = "),num
num_3 = num_1* num_1*num_1
print ("Cube  = "),num_3
cal_max(num, delay1)
cal_min(num_3,delay2)
print 'Elapsed time:', time.time() - start_time
start_time = time.time()
thread1 = threading.Thread(target=cal_max, args=(num, delay1,))
thread2 = threading.Thread(target=cal_min, args=(num_3, delay2,))
thread1.start()
thread2.start()
thread1.join()
thread2.join()
print 'Elapsed time:', time.time() - start_time

